let dark = false;

function getValue(className) {
    return getComputedStyle(document.documentElement).getPropertyValue(className);
}

function setValue(orgv, newv) {
    document.documentElement.style.setProperty(orgv, getValue(newv));
}

function setTheme() {    
    if (dark == false) {
        setValue('--dark-one', '--dark--dark-one');
        setValue('--dark-two', '--dark--dark-two');
        setValue('--mainColor', '--gColor');
        setValue('--bgColor', '--dark--bgColor');
        setValue('--conColor', '--dark--conColor');
        setValue('--text-mode', '--dark-mode');
        setValue('--conColor', '--dark--conColor');
        setValue('--radColor', '--dark--radColor');
        setValue('--textColor', '--dark--textColor');
        setValue('--btn', '--dark--btn');
        dark = true;
    }
    else {
        setValue('--dark-one', '--light--dark-one');
        setValue('--dark-two', '--light--dark-two');
        setValue('--mainColor', '--light--mainColor');
        setValue('--bgColor', '--light--bgColor');
        setValue('--conColor', '--light--conColor');
        setValue('--text-mode', '--light-mode');
        setValue('--conColor', '--light--conColor');
        setValue('--radColor', '--light--radColor');
        setValue('--textColor', '--light--textColor');
        setValue('--btn', '--light--btn');
        dark = false;
    }
}

function addButtonClickListener(className, callback) {
    const buttons = document.querySelectorAll('.' + className);
    buttons.forEach(button => {
        button.addEventListener('click', callback);
    });
}

addButtonClickListener('join', setTheme);